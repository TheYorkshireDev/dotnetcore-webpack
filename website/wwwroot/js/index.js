var carouselImage1Path = require("../images/banner1.svg");
var carouselImage2Path = require("../images/banner2.svg");
var carouselImage3Path = require("../images/banner3.svg");
var carouselImage4Path = require("../images/banner4.svg");

// Setting images for carousel
var carouselImage1 = document.querySelector('[data-src="carousel-img1"]');
carouselImage1.src = carouselImage1Path;
var carouselImage2 = document.querySelector('[data-src="carousel-img2"]');
carouselImage2.src = carouselImage2Path;
var carouselImage3 = document.querySelector('[data-src="carousel-img3"]');
carouselImage3.src = carouselImage3Path;
var carouselImage4 = document.querySelector('[data-src="carousel-img4"]');
carouselImage4.src = carouselImage4Path;
