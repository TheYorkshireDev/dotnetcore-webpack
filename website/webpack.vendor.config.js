var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

//Extract css into its own bundle site.css in /dist => https://github.com/webpack-contrib/extract-text-webpack-plugin
var extractCSS = new ExtractTextPlugin({
  filename: "vendor.bundle.css"
});

module.exports = (env) => {
  var isDevBuild = !(env && env.prod);
  return {
    mode: isDevBuild ? 'development' : 'production',
    entry: {
      vendor: ['jquery' ,'bootstrap', 'bootstrap/dist/css/bootstrap.css']
    },
    output: {
      publicPath: "/dist/",
      path: path.join(__dirname, 'wwwroot', 'dist'),
      filename: 'vendor.bundle.js',
      library: '[name]_[hash]'
    },
    module: {
      rules: [
        {
          test: /\.(png|jpg|jpeg|gif|woff|woff2|eot|ttf|svg)$/,
          // https://webpack.js.org/loaders/url-loader/
          use: [{
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: '[name].[ext]'
            }
          }]
        },
        {
          test: /\.css$/,
          use: extractCSS.extract({
            fallback: "style-loader",
            use: 'css-loader'
          })
        }
      ]
    },
    plugins: [
      extractCSS,
      //ProvidePlugin => https://webpack.js.org/plugins/provide-plugin/#usage-jquery
      //Maps these identifiers to the jQuery package (because Bootstrap expects it to be a global variable)
      new webpack.ProvidePlugin({ $: 'jquery', jQuery: 'jquery' }),
      //Split bundles further to improve performance => https://webpack.js.org/plugins/dll-plugin/
      new webpack.DllPlugin({
        path: path.join(__dirname, 'wwwroot', 'dist', '[name]-manifest.json'),
        name: '[name]_[hash]'
      }),
      //Create global constants => https://webpack.js.org/plugins/define-plugin/
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': isDevBuild ? '"development"' : '"production"'
      })
    ].concat(isDevBuild ? [] : [
      //Minifies and compresses the extracted css bundle => https://github.com/NMFR/optimize-css-assets-webpack-plugin
      new OptimizeCssAssetsPlugin({
        cssProcessorOptions: { discardComments: { removeAll: true } }
      })
    ])
  };
};
