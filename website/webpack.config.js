var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

//Extract css into its own bundle site.css in /dist => https://github.com/webpack-contrib/extract-text-webpack-plugin
var extractCSS = new ExtractTextPlugin({
  filename: "style.bundle.css"
});

module.exports = (env) => {
  var isDevBuild = !(env && env.prod);
  return {
    mode: isDevBuild ? 'development' : 'production',
    entry: {
      main: path.join(__dirname, 'wwwroot', 'js', 'main'),
      index: path.join(__dirname, 'wwwroot', 'js', 'index'),
      about: path.join(__dirname, 'wwwroot', 'js', 'about')
    },
    output: {
      publicPath: "/dist/",
      path: path.join(__dirname, 'wwwroot', 'dist'),
      filename: '[name].bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.(png|jpg|jpeg|gif|woff|woff2|eot|ttf|svg)$/,
          // https://webpack.js.org/loaders/url-loader/
          use: [{
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: '[name].[ext]'
            }
          }]
        },
        {
          test: /\.css$/,
          use: extractCSS.extract({
            fallback: "style-loader",
            use: 'css-loader'
          })
        }
      ]
    },
    plugins: [
      extractCSS,
      //Split bundles further to improve performance => https://webpack.js.org/plugins/dll-plugin/
      new webpack.DllReferencePlugin({
        manifest: path.join('wwwroot', 'dist', 'vendor-manifest.json')
      })
    ].concat(isDevBuild ? [] : [
      //Minifies and compresses the extracted css bundle => https://github.com/NMFR/optimize-css-assets-webpack-plugin
      new OptimizeCssAssetsPlugin({
        cssProcessorOptions: { discardComments: { removeAll: true } }
      })
    ])
  };
};
