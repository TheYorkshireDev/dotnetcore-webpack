# Adding Webpack to .Net Core 2.0 Website

A simple dotnet core web application that utilises webpack for bundling assets such as javascript, css and images. We aim to use webpack to produce a single `dist/` directory that contains all these assets for publishing.

Contents:
- [x] 0. Prerequisites
- [x] 1. Create Dotnet Core 2.0 project
- [x] 2. Prepare project for adding Webpack
- [x] 3. Install webpack
- [x] 4. Add first webpack config 
- [x] 5. Update build process to run webpack
- [x] 6. Create production and development config
- [x] 7. Add css to the config
- [x] 8. Add images and fonts to the config
- [x] 9. Add vendor config

## Prerequisites

* `Dotnet Core 2.0`: This project utilises dotnet core 2.0, we will use the cli to create the project and use the cli throughout for builds
* `yarn`/`npm`: We use a package manager for installing javascript libraries I used `yarn` but `npm` should work fine too.
* `Node` which should be installed anyway if you have either Yarn or NPM installed.

## Building and Running the Web Application

### Build in Debug
```
dotnet build
```
### Build in Release
```
dotnet build --configuration=Release
```
### Run in Debug
```
dotnet run
```
### Run in Release
```
dotnet run --configuration=Release
```
